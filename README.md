# 자동차금융 1차상담 관리 프로그램

<img src="https://www.hyundai.co.kr/image/upload/asset_library/MDA00000000000010976/fac87c86fadf4aef85f759f8fc55ae6c.jpg" width="450px" height="300px" title="px(픽셀) 크기 설정" alt="스팅어"></img><br/>


### version
```
0.0.1
```


#### Language
```
JAVA 16
springBoot 2.7.3
```

***

#### 기능
* 차량 정보 관리

|    __기능__    | __기능명__       |
|:------------:|:--------------|
| Manufacturer | 제조사 정보 (CRU)  |
|  Car Model   | 차량모델 정보 (CRU) |
|  Car Grade   | 차량등급 정보 (CRU) |
|  Car Trim   | 차량트림 정보 (CRU) |


* 상담일지 관리

| __기능__ | __기능명__  |
| :-----: |:---------|
| Consultation | 상담내역 정보 (CRU) |

***