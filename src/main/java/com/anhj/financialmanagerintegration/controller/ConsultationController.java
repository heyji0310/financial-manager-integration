package com.anhj.financialmanagerintegration.controller;

import com.anhj.financialmanagerintegration.entity.CarTrim;
import com.anhj.financialmanagerintegration.model.*;
import com.anhj.financialmanagerintegration.service.CarTotalService;
import com.anhj.financialmanagerintegration.service.ConsultationService;
import com.anhj.financialmanagerintegration.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "상담내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/consultation")
public class ConsultationController {
    private final CarTotalService carTotalService;
    private final ConsultationService consultationService;

    @ApiOperation(value = "상담내역 등록")
    @PostMapping("/new")
    public CommonResult setConsultation(@RequestBody @Valid ConsultationRequest request) {
        CarTrim carTrim = carTotalService.getCarTrimData(request.getCarTrimId());
        consultationService.setConsultation(carTrim, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "상담내역 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "상담내역 시퀀스", required = true)
    )
    @GetMapping("/{id}")
    public SingleResult<ConsultationDetail> getConsultation(@PathVariable long id) {
        return ResponseService.getSingleResult(consultationService.getConsultation(id));
    }

    @ApiOperation(value = "상담내역 전체조회")
    @GetMapping("/all")
    public ListResult<ConsultationItem> getConsultations() {
        return ResponseService.getListResult(consultationService.getConsultations(), true);
    }
}
