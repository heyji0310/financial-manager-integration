package com.anhj.financialmanagerintegration.controller;

import com.anhj.financialmanagerintegration.entity.CarGrade;
import com.anhj.financialmanagerintegration.entity.CarModel;
import com.anhj.financialmanagerintegration.entity.Manufacturer;
import com.anhj.financialmanagerintegration.enums.CarReleaseStatus;
import com.anhj.financialmanagerintegration.enums.ExternalType;
import com.anhj.financialmanagerintegration.enums.FuelType;
import com.anhj.financialmanagerintegration.enums.ManufacturerType;
import com.anhj.financialmanagerintegration.model.*;
import com.anhj.financialmanagerintegration.service.CarTotalService;
import com.anhj.financialmanagerintegration.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "차량 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car-manager")
public class CarTotalController {
    private final CarTotalService carTotalService;


    @ApiOperation(value = "차량 정보 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "차량모델 시퀀스", required = true)
    )
    @GetMapping("/car-total/{modelId}")
    public SingleResult<CarInfoDetailResponse> getCarInfoDetail(@PathVariable long modelId) {
        return ResponseService.getSingleResult(carTotalService.getCarInfoDetail(modelId));
    }


    // 1. manufacturer controller
    @ApiOperation(value = "1. 제조사 정보 등록")
    @PostMapping("/manufacturer/new")
    public CommonResult setManufacturer(@RequestBody @Valid ManufacturerRequest manufacturerRequest) {
        carTotalService.setManufacturer(manufacturerRequest);
        return ResponseService.getSuccessResult();
    }

//    @ApiOperation(value = "제조사 정보 조회")
//    @ApiImplicitParams(
//            @ApiImplicitParam(name = "id", value = "제조사 시퀀스", required = true)
//    )
//    @GetMapping("/manufacturer/{id}")
//    public SingleResult<ManufacturerDetail> getManufacturer(@PathVariable long id) {
//        return ResponseService.getSingleResult(carTotalService.getManufacturer(id));
//    }

    @ApiOperation(value = "제조사별 정보 조회")
    @GetMapping("/manufacturer/search")
    public ListResult<ManufacturerItem> getManufacturers(@RequestParam(value = "manufacturerType", required = false) ManufacturerType manufacturerType) {
        if (manufacturerType == null) {
            return ResponseService.getListResult(carTotalService.getManufacturers(), true);
        } else {
            return ResponseService.getListResult(carTotalService.getManufacturers(manufacturerType), true);
        }
    }

    @ApiOperation(value = "제조사 로고 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "제조사 시퀀스", required = true)
    )
    @PutMapping("/manufacturer/{id}")
    public CommonResult putManufacturerLogoImage(@PathVariable long id, @RequestBody @Valid ManufacturerLogoImageUpdateRequest updateRequest) {
        carTotalService.putManufacturerLogoImage(id, updateRequest);
        return ResponseService.getSuccessResult();
    }

    // 2. carModel controller
    @ApiOperation(value = "2. 차량 모델 등록")
    @PostMapping("/model/new")
    public CommonResult setCarModel(@RequestBody @Valid CarModelRequest modelRequest) {
        Manufacturer manufacturer = carTotalService.getManufacturerData(modelRequest.getManufacturerId());

        carTotalService.setCarModel(manufacturer, modelRequest);

        return ResponseService.getSuccessResult();
    }

//    @ApiOperation(value = "차량 모델 조회")
//    @ApiImplicitParams(
//            @ApiImplicitParam(name = "id", value = "차량모델 시퀀스", required = true)
//    )
//    @GetMapping("/model/{id}")
//    public SingleResult<CarModelDetail> getCarModel(@PathVariable long id) {
//        return ResponseService.getSingleResult(carTotalService.getCarModel(id));
//    }

    @ApiOperation(value = "차량 판매종류 별 조회")
    @GetMapping("/model/search")
    public ListResult<CarModelItem> getCarModels(@RequestParam(value = "carReleaseStatus", required = false)CarReleaseStatus carReleaseStatus) {
        if (carReleaseStatus == null) {
            return ResponseService.getListResult(carTotalService.getCarModels(), true);
        } else {
            return ResponseService.getListResult(carTotalService.getCarModels(carReleaseStatus), true);
        }
    }

    @ApiOperation(value = "차량 모델 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "차량모델 시퀀스", required = true)
    )
    @PutMapping("/model/{id}")
    public CommonResult putCarModelName(@PathVariable long id, @RequestBody @Valid CarModelNameUpdateRequest updateRequest) {
        carTotalService.putCarModelName(id, updateRequest);
        return ResponseService.getSuccessResult();
    }

    // 3. carGrade controller
    @ApiOperation(value = "3. 차량 등급 등록")
    @PostMapping("/grade/new")
    public CommonResult setCarGrade(@RequestBody @Valid CarGradeRequest gradeRequest) {
        CarModel carModel = carTotalService.getCarModelData(gradeRequest.getCarModelId());

        carTotalService.setCarGrade(carModel, gradeRequest);

        return ResponseService.getSuccessResult();
    }

//    @ApiOperation(value = "차량 등급 조회")
//    @ApiImplicitParams(
//            @ApiImplicitParam(name = "id", value = "차량등급 시퀀스", required = true)
//    )
//    @GetMapping("/grade/{id}")
//    public SingleResult<CarGradeDetail> getCarGrade(@PathVariable long id) {
//        return ResponseService.getSingleResult(carTotalService.getCarGrade(id));
//    }

    @ApiOperation(value = "연료타입별 조회")
    @GetMapping("/grade/search")
    public ListResult<CarGradeItem> getCarGrades(@RequestParam(value = "fuelType", required = false) FuelType fuelType) {
        if (fuelType == null) {
            return ResponseService.getListResult(carTotalService.getCarGrades(), true);
        } else {
            return ResponseService.getListResult(carTotalService.getCarGrades(fuelType), true);
        }

    }

    // 4. carTrim controller
    @ApiOperation(value = "4. 차량 트림 등록")
    @PostMapping("/trim/new")
    public CommonResult setCarTrim(@RequestBody @Valid CarTrimRequest trimRequest) {
        CarGrade carGrade = carTotalService.getCarGradeData(trimRequest.getCarGradeId());
        carTotalService.setCarTrim(carGrade, trimRequest);
        return ResponseService.getSuccessResult();
    }

//    @ApiOperation(value = "차량 트림 조회")
//    @ApiImplicitParams(
//            @ApiImplicitParam(name = "id", value = "차량트림 시퀀스", required = true)
//    )
//    @GetMapping("/trim/{id}")
//    public SingleResult<CarTrimDetail> getCarTrim(@PathVariable long id) {
//        return ResponseService.getSingleResult(carTotalService.getCarTrim(id));
//    }

    @ApiOperation(value = "차량 외장별 조회")
    @GetMapping("/trim/search")
    public ListResult<CarTrimItem> getCarTrims(@RequestParam(value = "externalType", required = false) ExternalType externalType) {
        if (externalType == null) {
            return ResponseService.getListResult(carTotalService.getCarTrims(), true);
        } else {
            return ResponseService.getListResult(carTotalService.getCarTrims(externalType), true);
        }
    }
}
