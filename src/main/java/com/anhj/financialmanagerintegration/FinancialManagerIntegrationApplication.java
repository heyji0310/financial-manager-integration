package com.anhj.financialmanagerintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinancialManagerIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinancialManagerIntegrationApplication.class, args);
	}

}
