package com.anhj.financialmanagerintegration.entity;

import com.anhj.financialmanagerintegration.enums.ExternalType;
import com.anhj.financialmanagerintegration.interfaces.CommonModelBuilder;
import com.anhj.financialmanagerintegration.model.CarTrimRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarTrim {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carGradeId", nullable = false)
    private com.anhj.financialmanagerintegration.entity.CarGrade carGrade;

    @Column(nullable = false, length = 30)
    private String trimName;

    @Column(nullable = false)
    private BigDecimal price;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private ExternalType externalType;

    @Column(nullable = false)
    private Double combinedFuel;

    @Column(nullable = false)
    private Double cityFuel;

    @Column(nullable = false)
    private Double highwayFuel;

    @Column(nullable = false)
    private Integer fuelGrade;

    @Column(nullable = false)
    private Integer capacity;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private CarTrim(CarTrimBuilder builder) {
        this.carGrade = builder.carGrade;
        this.trimName = builder.trimName;
        this.price = builder.price;
        this.externalType = builder.externalType;
        this.combinedFuel = builder.combinedFuel;
        this.cityFuel = builder.cityFuel;
        this.highwayFuel = builder.highwayFuel;
        this.fuelGrade = builder.fuelGrade;
        this.capacity = builder.capacity;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarTrimBuilder implements CommonModelBuilder<CarTrim> {
        private final com.anhj.financialmanagerintegration.entity.CarGrade carGrade;
        private final String trimName;
        private final BigDecimal price;
        private final ExternalType externalType;
        private final Double combinedFuel;
        private final Double cityFuel;
        private final Double highwayFuel;
        private final Integer fuelGrade;
        private final Integer capacity;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarTrimBuilder(com.anhj.financialmanagerintegration.entity.CarGrade carGrade, CarTrimRequest request) {
            this.carGrade = carGrade;
            this.trimName = request.getTrimName();
            this.price = request.getPrice();
            this.externalType = request.getExternalType();
            this.combinedFuel = request.getCombinedFuel();
            this.cityFuel = request.getCityFuel();
            this.highwayFuel = request.getHighwayFuel();
            this.fuelGrade = request.getFuelGrade();
            this.capacity = request.getCapacity();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarTrim build() {
            return new CarTrim(this);
        }
    }
}
