package com.anhj.financialmanagerintegration.entity;

import com.anhj.financialmanagerintegration.enums.ManufacturerType;
import com.anhj.financialmanagerintegration.interfaces.CommonModelBuilder;
import com.anhj.financialmanagerintegration.model.ManufacturerLogoImageUpdateRequest;
import com.anhj.financialmanagerintegration.model.ManufacturerRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Manufacturer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private ManufacturerType manufacturerType;

    @Column(nullable = false, length = 254)
    private String logoImage;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putLogoImage(ManufacturerLogoImageUpdateRequest updateRequest) {
        this.logoImage = updateRequest.getChangeLogoImage();
        this.dateUpdate = LocalDateTime.now();
    }

    private Manufacturer(ManufacturerBuilder builder) {
        this.manufacturerType = builder.manufacturerType;
        this.logoImage = builder.logoImage;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class ManufacturerBuilder implements CommonModelBuilder<Manufacturer> {
        private final ManufacturerType manufacturerType;
        private final String logoImage;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ManufacturerBuilder(ManufacturerRequest request) {
            this.manufacturerType = request.getManufacturerType();
            this.logoImage = request.getLogoImage();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Manufacturer build() {
            return new Manufacturer(this);
        }
    }
}
