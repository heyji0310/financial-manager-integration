package com.anhj.financialmanagerintegration.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarReleaseStatus {
    COMMERCIALIZATION("시판")
    , PLAN("예정")
    , DISCONTINUATION("단종")
    , UNDECIDED("미정")
    ;

    private final String carReleaseStatusName;
}
