package com.anhj.financialmanagerintegration.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ManufacturerType {
    KIA("기아")
    , HYUNDAI("현대")
    , SSANGYONG("쌍용")
    , RENAULT_KOREA("르노코리아")
    , CHEVROLET("쉐보레")
    , GENESIS("제네시스")
    , ECT("기타")
    ;

    private final String manufacturerTypeName;
}
