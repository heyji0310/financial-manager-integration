package com.anhj.financialmanagerintegration.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FuelType {
    GASOLINE("가솔린")
    , DIESEL("디젤")
    , LPG("LPG")
    , HYBRID("하이브리드")
    , ELECTRIC("전기차")
    , HYDROGEN("수소차")
    ;

    private final String fuelTypeName;
}
