package com.anhj.financialmanagerintegration.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExternalType {
    SEDAN("세단")
    , COUPE("쿠페")
    , HATCHBACK("해치백")
    , WAGON("왜건")
    , SUV("SUV")
    , VAN_RV("벤_RV")
    , CONVERTIBLE("컨버터블")
    , TRUCK("트럭")
    ;

    private final String externalTypeName;
}
