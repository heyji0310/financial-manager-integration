package com.anhj.financialmanagerintegration.repository;

import com.anhj.financialmanagerintegration.entity.CarModel;
import com.anhj.financialmanagerintegration.enums.CarReleaseStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {
    List<CarModel> findAllByCarReleaseStatusOrderByIdDesc(CarReleaseStatus carReleaseStatus);
}