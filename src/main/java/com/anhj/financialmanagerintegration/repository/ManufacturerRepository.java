package com.anhj.financialmanagerintegration.repository;

import com.anhj.financialmanagerintegration.entity.Manufacturer;
import com.anhj.financialmanagerintegration.enums.ManufacturerType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {
    List<Manufacturer> findAllByManufacturerTypeOrderByIdDesc(ManufacturerType manufacturerType);
}
