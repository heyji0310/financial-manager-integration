package com.anhj.financialmanagerintegration.repository;

import com.anhj.financialmanagerintegration.entity.CarGrade;
import com.anhj.financialmanagerintegration.enums.FuelType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarGradeRepository extends JpaRepository<CarGrade, Long> {
    List<CarGrade> findAllByFuelTypeOrderByIdDesc(FuelType fuelType);
}
