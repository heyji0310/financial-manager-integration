package com.anhj.financialmanagerintegration.repository;

import com.anhj.financialmanagerintegration.entity.CarTrim;
import com.anhj.financialmanagerintegration.enums.ExternalType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarTrimRepository extends JpaRepository<CarTrim, Long> {
    List<CarTrim> findAllByExternalTypeOrderByIdDesc(ExternalType externalType);

    List<CarTrim> findAllByCarGrade_CarModel_Id(Long carModelId);  // (trim_grade_model) 트림의 등급의 모델로 들어와서 "모델 Id"를 체크 / carTrim에서 carModelId를 가져올 수 있음
}
