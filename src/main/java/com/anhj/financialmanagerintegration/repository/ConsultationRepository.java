package com.anhj.financialmanagerintegration.repository;

import com.anhj.financialmanagerintegration.entity.Consultation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultationRepository extends JpaRepository<Consultation, Long> {
}
