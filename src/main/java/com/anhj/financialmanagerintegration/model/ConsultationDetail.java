package com.anhj.financialmanagerintegration.model;

import com.anhj.financialmanagerintegration.entity.Consultation;
import com.anhj.financialmanagerintegration.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConsultationDetail {
    private Long carTrimId;
    private String carTrimName;
    private String externalType;
    private String customerName;
    private String customerPhone;

    private ConsultationDetail(ConsultationDetailBuilder builder) {
        this.carTrimId = builder.carTrimId;
        this.carTrimName = builder.carTrimName;
        this.externalType = builder.externalType;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
    }

    public static class ConsultationDetailBuilder implements CommonModelBuilder<ConsultationDetail> {
        private final Long carTrimId;
        private final String carTrimName;
        private final String externalType;
        private final String customerName;
        private final String customerPhone;

        public ConsultationDetailBuilder(Consultation consultation) {
            this.carTrimId = consultation.getId();
            this.carTrimName = consultation.getCarTrim().getTrimName();
            this.externalType = consultation.getCarTrim().getExternalType().getExternalTypeName();
            this.customerName = consultation.getCustomerName();
            this.customerPhone = consultation.getCustomerPhone();
        }

        @Override
        public ConsultationDetail build() {
            return new ConsultationDetail(this);
        }
    }
}
