package com.anhj.financialmanagerintegration.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ManufacturerLogoImageUpdateRequest {
    @ApiModelProperty(notes = "로고 이미지")
    private String changeLogoImage;
}
