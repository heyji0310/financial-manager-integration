package com.anhj.financialmanagerintegration.model;

import com.anhj.financialmanagerintegration.enums.ExternalType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
public class CarTrimRequest {

    @ApiModelProperty(notes = "차량 등급 id", required = true)
    @NotNull
    private Long carGradeId;

    @ApiModelProperty(notes = "트림명", required = true)
    @NotNull
    @Length(max = 30)
    private String trimName;

    @ApiModelProperty(notes = "차량가격", required = true)
    @NotNull
    private BigDecimal price;

    @ApiModelProperty(notes = "외장형태", required = true)
    @NotNull
    private ExternalType externalType;

    @ApiModelProperty(notes = "복합연비", required = true)
    @NotNull
    private Double combinedFuel;

    @ApiModelProperty(notes = "도심연비", required = true)
    @NotNull
    private Double cityFuel;

    @ApiModelProperty(notes = "고속도로연비", required = true)
    @NotNull
    private Double highwayFuel;

    @ApiModelProperty(notes = "연비등급", required = true)
    @NotNull
    private Integer fuelGrade;

    @ApiModelProperty(notes = "정원", required = true)
    @NotNull
    private Integer capacity;
}
