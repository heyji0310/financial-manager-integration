package com.anhj.financialmanagerintegration.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarModelNameUpdateRequest {
    @ApiModelProperty(notes = "모델명")
    private String carModelName;
}
