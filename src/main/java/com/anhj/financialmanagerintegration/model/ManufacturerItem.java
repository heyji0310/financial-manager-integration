package com.anhj.financialmanagerintegration.model;

import com.anhj.financialmanagerintegration.entity.Manufacturer;
import com.anhj.financialmanagerintegration.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ManufacturerItem {
    @ApiModelProperty(notes = "제조사 시퀀스")
    private Long manufacturerId;

    @ApiModelProperty(notes = "제조사명(제조사명+로고이미지)")
    private String manufacturerLogoImageName;

    private ManufacturerItem(ManufacturerItemBuilder itemBuilder) {
        this.manufacturerId = itemBuilder.manufacturerId;
        this.manufacturerLogoImageName = itemBuilder.manufacturerLogoImageName;
    }

    public static class ManufacturerItemBuilder implements CommonModelBuilder<ManufacturerItem> {
        private final Long manufacturerId;
        private final String manufacturerLogoImageName;

        public ManufacturerItemBuilder(Manufacturer manufacturer) {
            this.manufacturerId = manufacturer.getId();
            this.manufacturerLogoImageName = "[" + manufacturer.getManufacturerType().getManufacturerTypeName() + "] " + manufacturer.getLogoImage();
        }

        @Override
        public ManufacturerItem build() {
            return new ManufacturerItem(this);
        }
    }
}
