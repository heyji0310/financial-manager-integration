package com.anhj.financialmanagerintegration.model;

import com.anhj.financialmanagerintegration.entity.Manufacturer;
import com.anhj.financialmanagerintegration.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ManufacturerDetail {

    @ApiModelProperty(notes = "제조사 시퀀스")
    private Long manufacturerId;

    @ApiModelProperty(notes = "제조사명")
    private String manufacturerName;

    @ApiModelProperty(notes = "제조사 로고이미지")
    private String logoImage;

    private ManufacturerDetail(ManufacturerDetailBuilder builder) {
        this.manufacturerId = builder.manufacturerId;
        this.manufacturerName = builder.manufacturerName;
        this.logoImage = builder.logoImage;
    }

    public static class ManufacturerDetailBuilder implements CommonModelBuilder<ManufacturerDetail> {
        private final Long manufacturerId;
        private final String manufacturerName;
        private final String logoImage;

        public ManufacturerDetailBuilder(Manufacturer manufacturer) {
            this.manufacturerId = manufacturer.getId();
            this.manufacturerName = manufacturer.getManufacturerType().getManufacturerTypeName();
            this.logoImage = manufacturer.getLogoImage();
        }

        @Override
        public ManufacturerDetail build() {
            return new ManufacturerDetail(this);
        }
    }
}
