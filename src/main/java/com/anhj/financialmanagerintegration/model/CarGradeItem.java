package com.anhj.financialmanagerintegration.model;

import com.anhj.financialmanagerintegration.entity.CarGrade;
import com.anhj.financialmanagerintegration.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarGradeItem {
    @ApiModelProperty(notes = "차량등급 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "차량모델 풀네임")
    private String carModelFullName;

    @ApiModelProperty(notes = "차량등급 풀네임")
    private String carGradeFullName;

    private CarGradeItem(CarGradeItemBuilder builder) {
        this.id = builder.id;
        this.carModelFullName = builder.carModelFullName;
        this.carGradeFullName = builder.carGradeFullName;
    }

    public static class CarGradeItemBuilder implements CommonModelBuilder<CarGradeItem> {
        private final Long id;
        private final String carModelFullName;
        private final String carGradeFullName;

        public CarGradeItemBuilder(CarGrade carGrade) {
            this.id = carGrade.getId();
            this.carModelFullName = "[" + carGrade.getCarModel().getManufacturer().getManufacturerType().getManufacturerTypeName() + "] " + carGrade.getCarModel().getModelName() + ", " + carGrade.getCarModel().getCreateYear().getYear() + "연식";
            this.carGradeFullName = carGrade.getGradeName() + "[" + carGrade.getFuelType().getFuelTypeName() + "] " + carGrade.getDisplacement() + "cc";
        }

        @Override
        public CarGradeItem build() {
            return new CarGradeItem(this);
        }
    }
}
