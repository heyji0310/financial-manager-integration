package com.anhj.financialmanagerintegration.model;

import com.anhj.financialmanagerintegration.entity.CarTrim;
import com.anhj.financialmanagerintegration.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarListItem {
    private Long carModelId;
    private String carReleaseStatusName;
    private String carImageUrl;
    private String carFullName;
    private String carPrice;

    private CarListItem(CarListItemBuilder builder) {
        this.carModelId = builder.carModelId;
        this.carReleaseStatusName = builder.carReleaseStatusName;
        this.carImageUrl = builder.carImageUrl;
        this.carFullName = builder.carFullName;
        this.carPrice = builder.carPrice;
    }

    public static class CarListItemBuilder implements CommonModelBuilder<CarListItem> {
        private final Long carModelId;
        private final String carReleaseStatusName;
        private final String carImageUrl;
        private final String carFullName;
        private final String carPrice;

        public CarListItemBuilder(CarTrim carTrim) {
            this.carModelId = carTrim.getCarGrade().getCarModel().getId();
            this.carReleaseStatusName = carTrim.getCarGrade().getCarModel().getCarReleaseStatus().getCarReleaseStatusName();
            this.carImageUrl = carTrim.getCarGrade().getCarModel().getCarModelImageUrl();
            this.carFullName = carTrim.getCarGrade().getCarModel().getManufacturer().getManufacturerType().getManufacturerTypeName() + " " + carTrim.getCarGrade().getCarModel().getModelName();
            this.carPrice = (carTrim.getPrice().intValue() / 10000) + "만원";
        }

        @Override
        public CarListItem build() {
            return new CarListItem(this);
        }
    }
}
