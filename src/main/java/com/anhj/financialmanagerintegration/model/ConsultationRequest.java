package com.anhj.financialmanagerintegration.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ConsultationRequest {
    @ApiModelProperty(notes = "차량 트림 id", required = true)
    @NotNull
    private Long carTrimId;

    @ApiModelProperty(notes = "고객명", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @ApiModelProperty(notes = "고객연락처", required = true)
    @NotNull
    @Length(min = 12, max = 15)
    private String customerPhone;
}
