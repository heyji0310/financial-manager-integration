package com.anhj.financialmanagerintegration.model;

import com.anhj.financialmanagerintegration.enums.ManufacturerType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ManufacturerRequest {
    @ApiModelProperty(notes = "제조사이름")
    @NotNull
    private ManufacturerType manufacturerType;

    @ApiModelProperty(notes = "로고 이미지")
    @NotNull
    @Length(max = 254)
    private String logoImage;
}
