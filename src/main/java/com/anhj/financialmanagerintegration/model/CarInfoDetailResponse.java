package com.anhj.financialmanagerintegration.model;

import com.anhj.financialmanagerintegration.entity.CarModel;
import com.anhj.financialmanagerintegration.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarInfoDetailResponse { // 모델 안에 모델을 사용하기 때문에 상위 모델에 "Response"명칭을 사용
    private String makerLogoUrl;
    private String carReleaseStatusName;
    private CarInfoDetailRangeItem priceItem;
    private String carType;
    private List<String> carFuelTypes;
    private CarInfoDetailRangeItem displacementItem;
    private CarInfoDetailRangeItem fuelEfficiencyItem;
    private CarInfoDetailRangeItem capacityItem;

    private CarInfoDetailResponse(CarInfoDetailResponseBuilder builder) {
        this.makerLogoUrl = builder.makerLogoUrl;
        this.carReleaseStatusName = builder.carReleaseStatusName;
        this.priceItem = builder.priceItem;
        this.carType = builder.carType;
        this.carFuelTypes = builder.carFuelTypes;
        this.displacementItem = builder.displacementItem;
        this.fuelEfficiencyItem = builder.fuelEfficiencyItem;
        this.capacityItem = builder.capacityItem;
    }

    public static class CarInfoDetailResponseBuilder implements CommonModelBuilder<CarInfoDetailResponse> {
        private final String makerLogoUrl;
        private final String carReleaseStatusName;
        private final CarInfoDetailRangeItem priceItem;
        private final String carType;
        private final List<String> carFuelTypes;
        private final CarInfoDetailRangeItem displacementItem;
        private final CarInfoDetailRangeItem fuelEfficiencyItem;
        private final CarInfoDetailRangeItem capacityItem;

        public CarInfoDetailResponseBuilder(
                CarModel carModel,
                BigDecimal minPrice,
                BigDecimal maxPrice,
                List<String> carFuelTypes,   // 리스트 형식으로 연료타입 받아오기
                Integer minDisplacement,
                Integer maxDisplacement,
                Double minFuelEfficiencyItem,
                Double maxFuelEfficiencyItem,
                Integer minCapacityItem,
                Integer maxCapacityItem
        ) {
            this.makerLogoUrl = carModel.getManufacturer().getLogoImage();
            this.carReleaseStatusName = carModel.getCarReleaseStatus().getCarReleaseStatusName();

            CarInfoDetailRangeItem priceItem = new CarInfoDetailRangeItem();
            priceItem.setMinBigDecimalValue(minPrice);
            priceItem.setMaxBigDecimalValue(maxPrice);
            this.priceItem = priceItem;

            this.carType = carModel.getModelName();

            this.carFuelTypes = carFuelTypes;

            CarInfoDetailRangeItem displacementItem = new CarInfoDetailRangeItem();
            displacementItem.setMinIntValue(minDisplacement);
            displacementItem.setMaxIntValue(maxDisplacement);
            this.displacementItem = displacementItem;

            CarInfoDetailRangeItem fuelEfficiencyItem = new CarInfoDetailRangeItem();
            fuelEfficiencyItem.setMinDoubleValue(minFuelEfficiencyItem);
            fuelEfficiencyItem.setMaxDoubleValue(maxFuelEfficiencyItem);
            this.fuelEfficiencyItem = fuelEfficiencyItem;

            CarInfoDetailRangeItem capacityItem = new CarInfoDetailRangeItem();
            capacityItem.setMinIntValue(minCapacityItem);
            capacityItem.setMaxIntValue(maxCapacityItem);
            this.capacityItem = capacityItem;
        }

        @Override
        public CarInfoDetailResponse build() {
            return new CarInfoDetailResponse(this);
        }
    }
}
