package com.anhj.financialmanagerintegration.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class CarInfoDetailRangeItem { // 필요한 부분만 사용하기 위해 builder를 사용하지 않음
    private Integer minIntValue;

    private Integer maxIntValue;

    private Double minDoubleValue;

    private Double maxDoubleValue;

    private BigDecimal minBigDecimalValue;

    private BigDecimal maxBigDecimalValue;
}
