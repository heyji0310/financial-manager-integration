package com.anhj.financialmanagerintegration.service;

import com.anhj.financialmanagerintegration.entity.CarGrade;
import com.anhj.financialmanagerintegration.entity.CarModel;
import com.anhj.financialmanagerintegration.entity.CarTrim;
import com.anhj.financialmanagerintegration.entity.Manufacturer;
import com.anhj.financialmanagerintegration.enums.CarReleaseStatus;
import com.anhj.financialmanagerintegration.enums.ExternalType;
import com.anhj.financialmanagerintegration.enums.FuelType;
import com.anhj.financialmanagerintegration.enums.ManufacturerType;
import com.anhj.financialmanagerintegration.exception.CMissingDataException;
import com.anhj.financialmanagerintegration.model.*;
import com.anhj.financialmanagerintegration.repository.CarGradeRepository;
import com.anhj.financialmanagerintegration.repository.CarModelRepository;
import com.anhj.financialmanagerintegration.repository.CarTrimRepository;
import com.anhj.financialmanagerintegration.repository.ManufacturerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
@RequiredArgsConstructor
public class CarTotalService {
    private final ManufacturerRepository manufacturerRepository;
    private final CarModelRepository carModelRepository;
    private final CarGradeRepository carGradeRepository;
    private final CarTrimRepository carTrimRepository;


    public CarInfoDetailResponse getCarInfoDetail(long modelId) {
        CarModel carModel = carModelRepository.findById(modelId).orElseThrow(CMissingDataException::new);

        List<CarTrim> trims = carTrimRepository.findAllByCarGrade_CarModel_Id(carModel.getId()); // 이미 carModel을 알고있기 때문에 Long modelId가 아닌 CarModel의 carModel.getId

        // Double price
        ArrayList<BigDecimal> prices = new ArrayList<>();
        trims.forEach(trim -> {
            prices.add(trim.getPrice());
        });
        prices.sort(Collections.reverseOrder()); // 내림차순으로 정렬 올림차순은 naturalOrder()
        BigDecimal maxPrice = prices.get(0);
        BigDecimal minPrice = prices.get(prices.size() - 1); // 컴퓨터는 0부터 시작, 5개가 있다고 하면 = (0, 1, 2, 3, 4)

        // String carFuelTypes
        List<String> carFuelTypes = new LinkedList<>();
        trims.forEach(trim -> {
            carFuelTypes.add(trim.getExternalType().getExternalTypeName());
        });
        Set<String> tempSet = new HashSet<>(carFuelTypes); // set은 중복을 허용하지 않음 / 참고사항- list, get 중복 허용
        List<String> carFuelTypesResult = new ArrayList<>(tempSet); // model<response>에서 원래 List<String>였고, 중복된 value들을 골라내기위해 set<String>으로 변환해서 사용
        // 후에 response 와 동일하게 List<String>으로 변환해줌

        // Integer displacement
        ArrayList<Integer> displacement = new ArrayList<>();
        trims.forEach(trim -> {
            displacement.add(trim.getCarGrade().getDisplacement());
        });
        displacement.sort(Collections.reverseOrder());
        Integer maxDisplacement = displacement.get(0);
        Integer minDisplacement = displacement.get(displacement.size() - 1);

        // Double fuelEfficiency
        ArrayList<Double> fuelEfficiencies = new ArrayList<>();
        trims.forEach(trim -> {
            fuelEfficiencies.add(trim.getCombinedFuel());
        });
        fuelEfficiencies.sort(Collections.reverseOrder());
        Double maxFuelEfficiencies = fuelEfficiencies.get(0);
        Double minFuelEfficiencies = fuelEfficiencies.get(fuelEfficiencies.size() - 1);

        // Integer capacityItem
        ArrayList<Integer> capacity = new ArrayList<>();
        trims.forEach(trim -> {
            capacity.add(trim.getCapacity());
        });
        capacity.sort(Collections.reverseOrder());
        Integer maxCapacity = capacity.get(0);
        Integer minCapacity = capacity.get(capacity.size() - 1);

        return new CarInfoDetailResponse.CarInfoDetailResponseBuilder(
                carModel,
                minPrice,
                maxPrice,
                carFuelTypesResult,
                minDisplacement,
                maxDisplacement,
                minFuelEfficiencies,
                maxFuelEfficiencies,
                minCapacity,
                maxCapacity
        ).build();
    }


    // 1. manufacturer service
    public Manufacturer getManufacturerData(long id) {
        return manufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setManufacturer(ManufacturerRequest request) {
        Manufacturer manufacturer = new Manufacturer.ManufacturerBuilder(request).build();
        manufacturerRepository.save(manufacturer);
    }

//    public ManufacturerDetail getManufacturer(long id) {
//        Manufacturer manufacturer = manufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
//        return new ManufacturerDetail.ManufacturerDetailBuilder(manufacturer).build();
//    }

    public ListResult<ManufacturerItem> getManufacturers() {
        List<Manufacturer> manufacturers = manufacturerRepository.findAll();
        List<ManufacturerItem> result =new LinkedList<>();
        manufacturers.forEach(manufacturer -> {
            ManufacturerItem addItem = new ManufacturerItem.ManufacturerItemBuilder(manufacturer).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<ManufacturerItem> getManufacturers(ManufacturerType manufacturerType) {
        List<Manufacturer> manufacturers = manufacturerRepository.findAllByManufacturerTypeOrderByIdDesc(manufacturerType);
        List<ManufacturerItem> result =new LinkedList<>();
        manufacturers.forEach(manufacturer -> {
            ManufacturerItem addItem = new ManufacturerItem.ManufacturerItemBuilder(manufacturer).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public void putManufacturerLogoImage(long id, ManufacturerLogoImageUpdateRequest updateRequest) {
        Manufacturer manufacturer = manufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
        manufacturer.putLogoImage(updateRequest);
        manufacturerRepository.save(manufacturer);
    }

    // 2. carModel service
    public CarModel getCarModelData(long id) {
        return carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setCarModel(Manufacturer manufacturer, CarModelRequest request) {
        CarModel carModel = new CarModel.CarModelBuilder(manufacturer, request).build();
        carModelRepository.save(carModel);
    }

//    public CarModelDetail getCarModel(long id) {
//        CarModel carModel = carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
//        return new CarModelDetail.CarModelDetailBuilder(carModel).build();
//    }

    public ListResult<CarModelItem> getCarModels() {
        List<CarModel> carModels = carModelRepository.findAll();
        List<CarModelItem> result = new LinkedList<>();

        carModels.forEach(carModel -> {
            CarModelItem addItem = new CarModelItem.CarModelItemBuilder(carModel).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<CarModelItem> getCarModels(CarReleaseStatus carReleaseStatus) {
        List<CarModel> carModels = carModelRepository.findAllByCarReleaseStatusOrderByIdDesc(carReleaseStatus);
        List<CarModelItem> result = new LinkedList<>();

        carModels.forEach(carModel -> {
            CarModelItem addItem = new CarModelItem.CarModelItemBuilder(carModel).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public void putCarModelName(long id, CarModelNameUpdateRequest updateRequest) {
        CarModel carModel = carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
        carModel.putCarModelNameData(updateRequest);
        carModelRepository.save(carModel);
    }

    // 3. carGrade service
    public CarGrade getCarGradeData(long id) {
        return carGradeRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setCarGrade(CarModel carModel, CarGradeRequest request) {
        CarGrade carGrade = new CarGrade.CarGradeBuilder(carModel, request).build();
        carGradeRepository.save(carGrade);
    }

//    public CarGradeDetail getCarGrade(long id) {
//        CarGrade carGrade = carGradeRepository.findById(id).orElseThrow(CMissingDataException::new);
//        return new CarGradeDetail.CarGradeDetailBuilder(carGrade).build();
//    }
//
    public ListResult<CarGradeItem> getCarGrades() {
        List<CarGrade> carGrades = carGradeRepository.findAll();
        List<CarGradeItem> result = new LinkedList<>();

        carGrades.forEach(carGrade -> {
            CarGradeItem addItem = new CarGradeItem.CarGradeItemBuilder(carGrade).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<CarGradeItem> getCarGrades(FuelType fuelType) {
        List<CarGrade> carGrades = carGradeRepository.findAllByFuelTypeOrderByIdDesc(fuelType);
        List<CarGradeItem> result = new LinkedList<>();

        carGrades.forEach(carGrade -> {
            CarGradeItem addItem = new CarGradeItem.CarGradeItemBuilder(carGrade).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    // 4. carTrim service
    public CarTrim getCarTrimData(long id) {
        return carTrimRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setCarTrim(CarGrade carGrade, CarTrimRequest request) {
        CarTrim carTrim = new CarTrim.CarTrimBuilder(carGrade, request).build();
        carTrimRepository.save(carTrim);
    }

//    public CarTrimDetail getCarTrim(long id) {
//        CarTrim carTrim = carTrimRepository.findById(id).orElseThrow(CMissingDataException::new);
//        return new CarTrimDetail.CarTrimDetailBuilder(carTrim).build();
//    }
//
    public ListResult<CarTrimItem> getCarTrims() {
        List<CarTrim> carTrims = carTrimRepository.findAll();
        List<CarTrimItem> result = new LinkedList<>();

        carTrims.forEach(carTrim -> {
            CarTrimItem addItem = new CarTrimItem.CarTrimItemBuilder(carTrim).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    public ListResult<CarTrimItem> getCarTrims(ExternalType externalType) {
        List<CarTrim> carTrims = carTrimRepository.findAllByExternalTypeOrderByIdDesc(externalType);
        List<CarTrimItem> result = new LinkedList<>();

        carTrims.forEach(carTrim -> {
            CarTrimItem addItem = new CarTrimItem.CarTrimItemBuilder(carTrim).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
