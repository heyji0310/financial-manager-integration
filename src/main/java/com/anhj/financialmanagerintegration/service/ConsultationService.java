package com.anhj.financialmanagerintegration.service;

import com.anhj.financialmanagerintegration.entity.CarTrim;
import com.anhj.financialmanagerintegration.entity.Consultation;
import com.anhj.financialmanagerintegration.exception.CMissingDataException;
import com.anhj.financialmanagerintegration.model.ConsultationDetail;
import com.anhj.financialmanagerintegration.model.ConsultationRequest;
import com.anhj.financialmanagerintegration.model.ConsultationItem;
import com.anhj.financialmanagerintegration.model.ListResult;
import com.anhj.financialmanagerintegration.repository.ConsultationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConsultationService {
    private final ConsultationRepository consultationRepository;

    public void setConsultation(CarTrim carTrim, ConsultationRequest request) {
        Consultation consultationDetail = new Consultation.ConsultationBuilder(carTrim, request).build();
        consultationRepository.save(consultationDetail);
    }

    public ConsultationDetail getConsultation (long id) {
        Consultation consultation = consultationRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new ConsultationDetail.ConsultationDetailBuilder(consultation).build();
    }

    public ListResult<ConsultationItem> getConsultations() {
        List<Consultation> consultations = consultationRepository.findAll();
        List<ConsultationItem> result = new LinkedList<>();

        consultations.forEach(consultation -> {
            ConsultationItem addItem = new ConsultationItem.ConsultationItemBuilder(consultation).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
}
